//
//  QuestionsModel.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import Foundation
import ObjectMapper

class AnswerModel: Mappable {
    
    var id: Int?
    var text: String?
    var correct: Bool?
    var order: Int?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        text <- map["text"]
        correct <- map["correct"]
        order <- map["order"]
    }
}

class QuestionsModel: Mappable {
    
    var id: Int?
    var text: String?
    var order: Int?
    var answers: [AnswerModel]?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        text <- map["text"]
        order <- map["order"]
        answers <- map["answers"]
    }
}

class AnswerResultModel: NSObject, Mappable {
    
    var question_id: Int?
    var answer_id: Int?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        question_id <- map["question_id"]
        answer_id <- map["answer_id"]
    }
}

class QuestionResultModel: NSObject, Mappable {
    
    var questionnaire_id: Int?
    var questionnaire_answers_attributes: [AnswerResultModel]?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        questionnaire_id <- map["questionnaire_id"]
        questionnaire_answers_attributes <- map["questionnaire_answers_attributes"]
    }
}
