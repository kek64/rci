//
//  QuestionnariesModel.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import Foundation
import ObjectMapper

class QuestionnariesModel: Mappable {
    
    var id: Int?
    var title: String?
    var description: String?
    var created_at: String?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        description <- map["description"]
        created_at <- map["created_at"]
    }
}
