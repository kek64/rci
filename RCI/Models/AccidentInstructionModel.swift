//
//  AccidentInstructionModel.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import Foundation
import ObjectMapper

class AccidentInstructionModel: Mappable {
    
    var id: Int?
    var title: String?
    var tabs: Bool?
    var tab_title1: String?
    var tab_content1: String?
    var tab_title2: String?
    var tab_content2: String?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        tabs <- map["tabs"]
        tab_title1 <- map["tab_1_title"]
        tab_content1 <- map["tab_1_content"]
        tab_title2 <- map["tab_2_title"]
        tab_content2 <- map["tab_2_content"]
    }
}
