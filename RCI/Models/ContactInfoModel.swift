//
//  ContactInfoModel.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import Foundation
import ObjectMapper

class ContactInfoModel: Mappable {
    
    var phone: String?
    var royal_payment_website: String?
    var e_nsured_website: String?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        phone <- map["phone"]
        royal_payment_website <- map["royal_payment_website"]
        e_nsured_website <- map["e_nsured_website"]
    }
}
