//
//  TextInfoModel.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import Foundation
import ObjectMapper

class TextInfoModel: Mappable {
    
    var infoText: String?
    var errors: [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        infoText <- map["about_us"]
        infoText <- map["about_royal_assist"]
        infoText <- map["message"]
        errors   <- map["errors"]
    }
}
