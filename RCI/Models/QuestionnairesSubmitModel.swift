//
//  QuestionnairesSubmitModel.swift
//  RCI
//
//  Created by Admin on 01.04.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import Foundation
import ObjectMapper

class QuestionnairesSubmitModel: Mappable {

    var score: String?
    var message: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        score <- map["score"]
        message <- map["message"]
    }
}
