//
//  ServiceModel.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import Foundation
import ObjectMapper

class ServiceModel: Mappable {
    
    var id: Int?
    var title: String?
    var website: String?
    var type: String?
    var description: String?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        website <- map["website"]
        type <- map["type"]
        description <- map["description"]
    }
}
