//
//  BranchModel.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import Foundation
import ObjectMapper

class BranchModel: Mappable {
    
    var id: Int?
    var title: String?
    var address: String?
    var phone: String?
    var fax: String?
    var email: String?
    var postal_code: String?
    var latitude: Double?
    var longitude: Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        address <- map["address"]
        phone <- map["phone"]
        fax <- map["fax"]
        email <- map["email"]
        postal_code <- map["postal_code"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}
