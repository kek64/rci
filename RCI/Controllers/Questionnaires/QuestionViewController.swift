//
//  QuestionViewController.swift
//  RCI
//
//  Created by Admin on 02.04.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class AnswerCell: UITableViewCell {
    
    @IBOutlet weak var answerTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.answerTextLabel.text = ""
    }
    
}

protocol QuestionViewControllerDelegate {
    func didSelectAnswer(questionId: Int, answerId: Int)
}

class QuestionViewController: UIViewController {

    let reuseIdentifier = "answerCellReuseIdentifier"
    
    var model: QuestionsModel?
    var pageIndex: Int = 0
    var pageNum: Int = 0
    
    var delegate: QuestionViewControllerDelegate?
    
    var prevSelectedRow: Int = -1
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = self.view.backgroundColor
        self.tableView.estimatedRowHeight = 56.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        numberLabel.text = String(describing: pageIndex + 1) + "/" + String(pageNum)
        titleLabel.text = model?.text
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


// MARK: UITableViewDataSource

extension QuestionViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.model?.answers?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! AnswerCell
        
        let answer = self.model?.answers?[indexPath.row]
        cell.answerTextLabel?.text = answer?.text
        
        if answer?.id == prevSelectedRow {
            cell.answerTextLabel?.textColor = .black
        } else {
            cell.answerTextLabel?.textColor = .gray
        }
        
        return cell
    }
    
}


// MARK: UITableViewDelegate

extension QuestionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        prevSelectedRow = (self.model?.answers?[indexPath.row].id)!
        self.delegate?.didSelectAnswer(questionId: (self.model?.id!)!, answerId: (self.model?.answers?[indexPath.row].id)!)
        tableView.reloadData()
    }
    
}

