//
//  ReviewAnswersViewController.swift
//  RCI
//
//  Created by Admin on 03.04.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class ReviewAnswerCell: UITableViewCell {
    
    @IBOutlet weak var numLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        numLabel.text = ""
        questionLabel.text = ""
        answerLabel.text = ""
    }
}

class ReviewAnswersViewController: UIViewController {
    
    var resultModel: QuestionnairesSubmitModel?
    var answers: [QuestionsModel]?
    
    let reuseIdentifier = "answerReviewCellReuseIdentifier"
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helper.addRightLogoImageToNavigationItem(target: self, action: nil)
        
        let backButton = UIBarButtonItem(image: UIImage(named: "back_icon"), style: .plain, target: self, action: #selector(actionBack))
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = backButton
        
        self.title = "Review answer"
        self.scoreLabel.text = resultModel?.score
        
        let mutableAttr = NSMutableAttributedString(attributedString: (resultModel?.message?.utf8Data?.attributedString)!)
        mutableAttr.addAttributes([NSForegroundColorAttributeName: UIColor.appGeneralColor(), NSFontAttributeName: UIFont.systemFont(ofSize: 15.0)], range: NSMakeRange(0, mutableAttr.length))
        self.messageLabel.attributedText = mutableAttr
        
        self.tableView.dataSource = self
        self.tableView.backgroundColor = self.view.backgroundColor
        self.tableView.estimatedRowHeight = 64.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func actionTryAgain(_ sender: Any) {
        let viewControllers = self.navigationController?.viewControllers
        let vc = viewControllers?[(viewControllers?.count)! - 2] as! TestsViewController
        vc.clearState()
        self.navigationController?.popViewController(animated: true)
    }
    
    func actionBack() {
        let viewControllers = self.navigationController?.viewControllers
        self.navigationController!.popToViewController((viewControllers?[(viewControllers?.count)! - 3])!, animated: true)
    }
}


// MARK: UITableViewDataSource

extension ReviewAnswersViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (answers?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ReviewAnswerCell
        
        let i = indexPath.row
        
        for k in 0 ..< (self.answers?[i].answers?.count)! {
            if (answers?[i].answers?[k].correct)! {
                let item = answers?[i].answers?[k]

                cell.numLabel.text = String(i + 1) + "/" + String(describing: (self.answers?.count)!)
                cell.questionLabel.text = "Q: " + (self.answers?[i].text)!
                cell.answerLabel.text = "A: " + (item?.text)!
                    
                break
            }
        }
        
        
        return cell
    }
    
}
