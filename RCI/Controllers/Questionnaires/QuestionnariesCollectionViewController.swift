//
//  QuestionnariesCollectionViewController.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit
import ObjectMapper

class QuestionnariesCollectionViewController: UICollectionViewController {

    let manager = NetworkManager.instance
    let reuseIdentifier = "QuestionnariesCellIdentifier"
    var items = [QuestionnariesModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register cell classes
        self.collectionView!.register(UINib.init(nibName: "QuestionCollectionViewCell", bundle: nil).self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView!.backgroundColor = UIColor.groupTableViewBackground
        self.collectionView!.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        
        Helper.addRightLogoImageToNavigationItem(target: self, action: nil)
        self.title = "Questionnaires"
        
        fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchData() {
        Helper.showSpinner(self)
        manager.getQuestionnaries(title: "") { (result) in
            Helper.hideSpinner(self)
            if result != nil {
                self.items = result!
                self.collectionView?.reloadData()
            } else {
                Helper.showError(self, message: "Something went wrong :(")
            }
        }
    }

}


// MARK: UICollectionViewDataSource

extension QuestionnariesCollectionViewController {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! QuestionCollectionViewCell
        
        // Configure the cell
        let item = self.items[indexPath.item]
        cell.titleLabel?.text = item.title
        cell.descriptionLabel?.text = item.description
        cell.setupShadows()
  
        return cell
    }

}


// MARK: UICollectionViewDelegate

extension QuestionnariesCollectionViewController {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TestsVC") as! TestsViewController
        vc.model = self.items[indexPath.item]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}



// MARK: UICollectionViewDelegateFlowLayout

extension QuestionnariesCollectionViewController : UICollectionViewDelegateFlowLayout {
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = QuestionCollectionViewCell.estimateCellHeight(title: self.items[indexPath.item].title!, text: self.items[indexPath.item].description!)
        return CGSize(width: UIScreen.main.bounds.size.width - 30, height: height)
    }
    
}
