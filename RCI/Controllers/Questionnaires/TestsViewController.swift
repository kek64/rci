//
//  TestsViewController.swift
//  RCI
//
//  Created by Admin on 02.04.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit
import ObjectMapper

class TestsViewController: UIViewController, QuestionViewControllerDelegate {

    let manager = NetworkManager.instance
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var nextButton: UIButton!
    
    var model: QuestionnariesModel?
    
    var pageViewController: UIPageViewController?
    var questionModels = [QuestionsModel]()
    var currentIndex: Int = 0
    
    var selectedAnswerIndex: Int = 0
    var selectedQuestionIndex: Int = 0
    
    var resultModel = QuestionResultModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = model?.title!
        resultModel.questionnaire_id = self.model?.id!
        resultModel.questionnaire_answers_attributes = [AnswerResultModel]()
        
        Helper.addRightLogoImageToNavigationItem(target: self, action: nil)
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)

        addChildViewController(pageViewController!)
        view.addSubview(pageViewController!.view)
        pageViewController?.didMove(toParentViewController: self)
        view.sendSubview(toBack: pageViewController!.view)
    
        fetchData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pageViewController!.view.frame = self.view.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchData() {
        // fetch question list from server
        Helper.showSpinner(self)
        manager.getQuestionsList(questtionaireId: (self.model?.id)!) { (result) in
            Helper.hideSpinner(self)
            if result != nil {
                self.questionModels = result!
                
                self.pageControl.numberOfPages = self.questionModels.count
                let vc = self.viewControllerAtIndex(index: self.currentIndex)!
                let viewControllers = [vc]
                self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: true, completion: nil)
                
            } else {
                Helper.showError(self, message: "Something went wrong :(")
            }
        }
    }
    
    @IBAction func actionPrevNavigate(_ sender: Any) {
        let vc = self.viewControllerAtIndex(index: self.currentIndex - 1)
        if vc == nil {
            return
        }
        
        // load back selected index here
        vc?.prevSelectedRow = (self.resultModel.questionnaire_answers_attributes?[self.currentIndex].answer_id!)!
        selectedAnswerIndex = (self.resultModel.questionnaire_answers_attributes?[self.currentIndex].answer_id!)!
        selectedQuestionIndex = (self.resultModel.questionnaire_answers_attributes?[self.currentIndex].question_id!)!
        
        let viewControllers = [vc]
        self.pageViewController!.setViewControllers(viewControllers as?[UIViewController], direction: .reverse, animated: true, completion: nil)
        self.pageControl.currentPage = currentIndex
    }
    
    @IBAction func actionNextNavigate(_ sender: Any) {
        
        if selectedQuestionIndex == 0 && selectedAnswerIndex == 0 {
            Helper.showError(self, message: "Select answer!")
            return
        }
        
        if self.currentIndex == self.questionModels.count - 2 {
            self.nextButton.setTitle("Finish", for: .normal)
        }
        
        // save selected answer here ...
        let attribute = AnswerResultModel()
        attribute.answer_id = self.selectedAnswerIndex
        attribute.question_id = self.selectedQuestionIndex
        if (self.resultModel.questionnaire_answers_attributes?.count)! <= self.currentIndex {
            self.resultModel.questionnaire_answers_attributes?.append(attribute)
        } else {
            self.resultModel.questionnaire_answers_attributes?[self.currentIndex] = attribute
        }
        
        let vc = self.viewControllerAtIndex(index: self.currentIndex + 1)
        if vc == nil {
            if currentIndex == self.questionModels.count - 1 {
                if self.nextButton.titleLabel?.text == "Finish" {
                    finishTest()
                }
            }
            return
        }
        
        selectedAnswerIndex = 0
        selectedQuestionIndex = 0
        
        if (self.resultModel.questionnaire_answers_attributes?.count)! > self.currentIndex {
            selectedAnswerIndex = (self.resultModel.questionnaire_answers_attributes?[self.currentIndex].answer_id!)!
            selectedQuestionIndex = (self.resultModel.questionnaire_answers_attributes?[self.currentIndex].question_id!)!
            vc?.prevSelectedRow = selectedAnswerIndex
        }
        
        let viewControllers = [vc]
        self.pageViewController!.setViewControllers(viewControllers as?[UIViewController], direction: .forward, animated: true, completion: nil)
        self.pageControl.currentPage = currentIndex
    }
    
    func viewControllerAtIndex(index: Int) -> QuestionViewController? {
        if (self.questionModels.count <= 0) || (index >= self.questionModels.count) || (index < 0) {
            return nil
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "QuestionVC") as! QuestionViewController
        vc.delegate = self
        vc.model = questionModels[index]
        vc.pageIndex = index
        vc.pageNum = self.questionModels.count
        
        self.pageViewController?.dataSource = nil
        
        currentIndex = index
        
        return vc
    }
    
    func didSelectAnswer(questionId: Int, answerId: Int) {
        self.selectedAnswerIndex = answerId
        self.selectedQuestionIndex = questionId
    }
    
    func finishTest() {
        Helper.showSpinner(self)
        manager.submitQuestionnaireResults(completedQuestions: self.resultModel) { (result) in
            Helper.hideSpinner(self)
            if result != nil {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewAnswersVC") as! ReviewAnswersViewController
                vc.resultModel = result!
                vc.answers = self.questionModels
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                Helper.showError(self, message: "Something went wrong!")
            }
        }
    }
    
    func clearState() {
        self.selectedQuestionIndex = 0
        self.selectedAnswerIndex = 0
        self.currentIndex = 0
        self.pageControl.currentPage = 0
        self.resultModel = QuestionResultModel()
        self.resultModel.questionnaire_id = self.model?.id!
        self.resultModel.questionnaire_answers_attributes = [AnswerResultModel]()
        self.nextButton.setTitle("Next", for: .normal)
        let vc = self.viewControllerAtIndex(index: self.currentIndex)!
        let viewControllers = [vc]
        self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: true, completion: nil)
    }
    
}

