//
//  RoyalAssistTableViewController.swift
//  RCI
//
//  Created by Admin on 27.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class RoyalAssistTableViewController: UITableViewController {
    
    var aboutModel: TextInfoModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.addRightLogoImageToNavigationItem(target: self, action: nil)
        self.title = "Royal Assist"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showReportAnAccident() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccidentReportVC") as! AccidentReportViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showMakeCall() {
        Helper.openURL(urlString: AppInfoStorage.instance.phone)
    }
    
    func showAboutRoyalAssist() {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TextInfoVC") as! TextInfoViewController
        vc.title = "About royal assist"
        
        if self.aboutModel != nil {
            vc.infoText = (aboutModel?.infoText!)!
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        Helper.showSpinner(self)
        NetworkManager.instance.getAboutRoyalAssist { (result) in
            Helper.hideSpinner(self)
            if result != nil {
                self.aboutModel = result!
                vc.infoText = result!.infoText!
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                Helper.showError(self, message: "Something went wrong :(")
            }
        }
        
    }
    
}


// MARK: UITableViewDelegate

extension RoyalAssistTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: // report an accident
            showReportAnAccident()
            break
        case 1: // make a call
            showMakeCall()
            break
        case 2: // about royal assist
            showAboutRoyalAssist()
            break
        default:
            break
        }
    }
    
}
