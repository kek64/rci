//
//  AccidentReportViewController.swift
//  RCI
//
//  Created by Admin on 30.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class AccidentReportViewController: UIViewController, AttachmentCollectionViewCellDelegate {

    @IBOutlet weak var nameTextField: MaterialTextField!
    @IBOutlet weak var policyNoTextField: MaterialTextField!
    @IBOutlet weak var phoneNumberTextField: MaterialTextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var agreeSwitch: UISwitch!
    @IBOutlet weak var addPhotoButton: UIButton!
    
    let reuseIdentifier = "attachmentCellReuseIdentifier"
    
    var attachments = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Accident report"
        Helper.addRightLogoImageToNavigationItem(target: self, action: nil)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib.init(nibName: "AttachmentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        self.agreeSwitch.setOn(false, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func actionDeleteAttachment(attachmentId: Int) {
        attachments.remove(at: attachmentId)
        self.collectionView.reloadData()
        //self.collectionView.deleteItems(at: [IndexPath(item: attachmentId, section: 0)])
    }

    @IBAction func actionAddPhoto(_ sender: Any) {
        addPhotoWithOptions()
    }
    
    @IBAction func actionChangeAgree(_ sender: Any) {
        //self.reportButton.isEnabled = self.agreeSwitch.isOn
    }
    
    @IBAction func actionReportAccident(_ sender: Any) {
        if !self.agreeSwitch.isOn {
            Helper.showError(self, message: "Need agree with Terms & Conditions")
            return
        }
        
        // request here ...
    }
    
}


// MARK: UIImagePickerDelegate

extension AccidentReportViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func addPhotoWithOptions() {
        let camera = Camera()
        camera.delegate = self
        
        let alertOptions = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertOptions.popoverPresentationController?.sourceView = self.view
        
        // photo library
        let photoLibraryOption = UIAlertAction(title: "Photo Library", style: .default) { (alert) in
            if !camera.presentPhotoLibrary(target: self, canEdit: false) {
                // some impl
            }
        }
        
        let cameraOption = UIAlertAction(title: "Camera", style: .default) { (alert) in
            if !camera.presentCamera(target: self, canEdit: false, cameraType: .rear) {
                // some impl
            }
        }
        
        let cancelOption = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertOptions.addAction(photoLibraryOption)
        alertOptions.addAction(cameraOption)
        alertOptions.addAction(cancelOption)
        
        self.present(alertOptions, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.attachments.append(image)
        self.collectionView.reloadData()
        self.collectionView.scrollToItem(at: IndexPath.init(row: self.attachments.count - 1, section: 0), at: .right, animated: true)
        picker.dismiss(animated: true, completion: nil)
    }
    
}


// MARK: UITextFieldDelegate

extension AccidentReportViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.nameTextField {
            self.policyNoTextField.becomeFirstResponder()
        }
        if textField == self.policyNoTextField {
            self.phoneNumberTextField.becomeFirstResponder()
        }
        
        return true
    }

}


// MARK: UICollectionViewDataSource

extension AccidentReportViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! AttachmentCollectionViewCell
        cell.backgroundImage.image = attachments[indexPath.item]
        cell.attachmentId = indexPath.item
        cell.delegate = self
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return attachments.count
    }
    
}


// MARK: UICollectionViewDelegate

extension AccidentReportViewController : UICollectionViewDelegate {
    
}





