//
//  MainCollectionViewController.swift
//  RCI
//
//  Created by Admin on 30.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class MenuCellModel {
    var title: String?
    var imageName: String?
    var viewControllerIdentifier: String?
    var link: String?
    
    init(title: String, imageName: String, viewControllerIdentifier: String) {
        self.title = title
        self.imageName = imageName
        self.viewControllerIdentifier = viewControllerIdentifier
    }
}

class MainCollectionViewController: UICollectionViewController {
    
    let reuseIdentifier = "mainCellReuseIdentifier"
    var menuItems = [MenuCellModel]()
    
    var cellSizes: [CGSize] = []
    let cellPadding: CGFloat = 5.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.register(UINib.init(nibName: "MainCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        Helper.addLogoImageToNavigationItem(self.navigationItem)
        
        createMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        calculateCellSizes()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func createMenu() {
        let royalAssistItem = MenuCellModel.init(title: "ROYAL ASSIST", imageName: "royal_assist_image", viewControllerIdentifier: "RoyalAssistVC")
        let royalPaymentItem = MenuCellModel.init(title: "ROYAL PAYMENT", imageName: "royal_payment_image", viewControllerIdentifier: "web_link")
        royalPaymentItem.link = AppInfoStorage.instance.royal_payment_website
        let servicesItem = MenuCellModel.init(title: "SERVICES", imageName: "services_image", viewControllerIdentifier: "ServicesVC")
        let whatToDoItem = MenuCellModel.init(title: "WHAT TO DO IF", imageName: "what_to_do_if_image", viewControllerIdentifier: "WhatToDoVC")
        let aboutItem = MenuCellModel.init(title: "ABOUT", imageName: "about_image", viewControllerIdentifier: "AboutVC")
        let questionnariesItem = MenuCellModel.init(title: "QUESTIONNAIRES", imageName: "services_image", viewControllerIdentifier: "QuestionnariesVC")
        
        self.menuItems.append(royalAssistItem)
        self.menuItems.append(royalPaymentItem)
        self.menuItems.append(servicesItem)
        self.menuItems.append(whatToDoItem)
        self.menuItems.append(aboutItem)
        self.menuItems.append(questionnariesItem)
    }
    
    func calculateCellSizes() {
        var counter = 0
        let cellWidth: CGFloat = UIScreen.main.bounds.size.width - (collectionView!.contentInset.left + collectionView!.contentInset.right)
        let particalWidth: CGFloat = (cellWidth / 2) - cellPadding
        let cellHeight: CGFloat = particalWidth + 16
        for _ in 0..<menuItems.count  {
            if counter == 2 {
                cellSizes.append(CGSize(width: cellWidth, height: cellHeight))
            } else {
                cellSizes.append(CGSize(width: particalWidth, height: cellHeight))
            }
            counter = (counter == 2) ? 0 : counter + 1
        }
    }
    
}


// MARK: UICollectionViewDataSource 

extension MainCollectionViewController {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MainCollectionViewCell
        
        // Configure the cell
        cell.backgroundImage?.image = UIImage(named: self.menuItems[indexPath.item].imageName!)
        cell.titleLabel?.text = self.menuItems[indexPath.item].title
        
        return cell
    }
    
}


// MARK: UICollectionViewDelegate

extension MainCollectionViewController {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.menuItems[indexPath.item].viewControllerIdentifier == "web_link" {
            Helper.openURL(urlString: self.menuItems[indexPath.item].link!)
            return
        }

        let vc = self.storyboard?.instantiateViewController(withIdentifier: self.menuItems[indexPath.item].viewControllerIdentifier!)
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
}


// MARK: UICollectionViewDelegateFlowLayout

extension MainCollectionViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSizes[indexPath.item]
    }
    
}
