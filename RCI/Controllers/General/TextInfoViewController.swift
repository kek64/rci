//
//  TextInfoViewController.swift
//  RCI
//
//  Created by Admin on 28.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class TextInfoViewController: UIViewController {
    
    @IBOutlet weak var infoTextView: UITextView!
    var infoText = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.addRightLogoImageToNavigationItem(target: self, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let mutableAttr = NSMutableAttributedString(attributedString: (infoText.utf8Data?.attributedString)!)
        mutableAttr.addAttributes([NSForegroundColorAttributeName: UIColor.appGeneralColor(), NSFontAttributeName: UIFont.systemFont(ofSize: 15.0)], range: NSMakeRange(0, mutableAttr.length))
        infoTextView.attributedText = mutableAttr
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


