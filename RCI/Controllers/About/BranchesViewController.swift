//
//  BranchesViewController.swift
//  RCI
//
//  Created by Admin on 28.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit
import GoogleMaps

class CustomMarker: GMSMarker {
    var model: BranchModel
    var index: Int
    
    init(model: BranchModel, index: Int) {
        self.model = model
        self.index = index
        
        super.init()
        
        self.icon = UIImage(named: "pin_passive_icon")
        self.position = CLLocationCoordinate2D(latitude: model.latitude!, longitude: model.longitude!)
    }

}

class BranchesViewController: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var detailsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var branchTitleLabel: UILabel!
    @IBOutlet weak var branchDescriptionLabel: UILabel!
    
    var markers = [CustomMarker]()
    var currentMarkerIndex: Int = 0
    var previoursMarkerIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.addRightLogoImageToNavigationItem(target: self, action: #selector(actionSelectNextMarker))
        self.title = "Branches"
        mapView.delegate = self
        branchDescriptionLabel.text = ""
        branchTitleLabel.text = ""
        self.view.backgroundColor = .white
        self.detailsView.backgroundColor = .white
        self.mapView.backgroundColor = .white
        self.branchDescriptionLabel.adjustsFontSizeToFitWidth = false
        fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func actionSelectNextMarker() {
        if currentMarkerIndex >= markers.count {
            currentMarkerIndex = 0
        }
        
        zoomToMarker(markers[currentMarkerIndex])
        displayItemInfo(markers[currentMarkerIndex])
        
        currentMarkerIndex += 1
    }
    
    func showMarkers(models: [BranchModel]) {
        for i in 0 ..< models.count {
            let marker = CustomMarker(model: models[i], index: i)
            marker.map = self.mapView
            markers.append(marker)
        }
        
        if markers.count == 0 {
            return
        }
        
        markers[0].icon = UIImage(named: "pin_active_icon")
        actionSelectNextMarker()
    }
    
    func fetchData() {
        Helper.showSpinner(self)
        NetworkManager.instance.getBranches { (result) in
            Helper.hideSpinner(self)
            if result != nil {
                self.showMarkers(models: result!)
            } else {
                Helper.showError(self, message: "Something went worng")
            }
        }
    }
  
    @IBAction func actionToogleDetailsView(_ sender: Any) {
        let button = sender as! UIButton
        let animSpeed = 0.5
        var offset: CGFloat = 0.0
        
        if button.tag == 0 {
            button.tag = 1
            offset = self.detailsView.bounds.size.height
            self.detailsViewHeightConstraint.constant += offset
            self.branchDescriptionLabel.adjustsFontSizeToFitWidth = true
            button.setImage(UIImage(named:"arrow_down_icon"), for: .normal)
        } else {
            button.tag = 0
            offset = self.detailsView.bounds.size.height / 2
            self.detailsViewHeightConstraint.constant -= offset
            self.branchDescriptionLabel.adjustsFontSizeToFitWidth = false
            button.setImage(UIImage(named:"arrow_up_icon"), for: .normal)
        }
        
        UIView.animate(withDuration: animSpeed, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func displayItemInfo(_ marker: CustomMarker) {
        
        // change marker icon
        if previoursMarkerIndex != marker.index {
            marker.icon = UIImage(named: "pin_active_icon")
            markers[previoursMarkerIndex].icon = UIImage(named: "pin_passive_icon")
            previoursMarkerIndex = marker.index
        }
        
        let item = marker.model
        
        branchTitleLabel.text = item.title
        
        var descriptionString: String = ""
        
        // address
        descriptionString.append(item.address!.replacingOccurrences(of: "\r", with: "") + "\n")
        
        // Postal code
        descriptionString.append("P: " + item.postal_code! + "\n")
        
        // Phone
        descriptionString.append("T: " + item.phone! + "\n")
        
        // Fax
        descriptionString.append("F: " + item.fax! + "\n")
        
        // Email
        descriptionString.append("E: " + item.email!)

        branchDescriptionLabel.text = descriptionString
    }
    
    func zoomToMarker(_ boundingMarker: GMSMarker) {
        if markers.count == 0 {
            return
        }
        
        var bounds = GMSCoordinateBounds(coordinate: boundingMarker.position, coordinate: boundingMarker.position)
        for marker in markers {
            bounds = bounds.includingCoordinate(marker.position)
        }
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds))
    }
}


// MARK: GMSMapViewDelegate

extension BranchesViewController : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let casted_marker = marker as! CustomMarker
        currentMarkerIndex = casted_marker.index + 1
        displayItemInfo(casted_marker)
        return true
    }
    
}
