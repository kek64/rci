//
//  AboutTableViewController.swift
//  RCI
//
//  Created by Admin on 27.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class AboutTableViewController: UITableViewController {
    
    var aboutModel: TextInfoModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.addRightLogoImageToNavigationItem(target: self, action: nil)
        self.title = "About"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showEnsured() {
        Helper.openURL(urlString: AppInfoStorage.instance.e_nsured_website)
    }
    
    func showBranches() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BranchesVC")
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func showAboutUs() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TextInfoVC") as! TextInfoViewController
        vc.title = "About us"
        
        if self.aboutModel != nil {
            vc.infoText = (self.aboutModel?.infoText!)!
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        Helper.showSpinner(self)
        NetworkManager.instance.getAboutUs { (result) in
            Helper.hideSpinner(self)
            if result != nil {
                self.aboutModel = result!
                vc.infoText = result!.infoText!
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                Helper.showError(self, message: "Something went wrong :(")
            }
        }
    }
}


// MARK: UITableViewDelegate

extension AboutTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: // about us
            showAboutUs()
            break
        case 1: // branches
            showBranches()
            break
        case 2: // e-nsured
            showEnsured()
            break
        default:
            break
        }
    }

}
