//
//  WhatToDoTableViewController.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class WhatToDoTableViewController: UITableViewController {

    let reuseIdentifier = "itemsReuseIdentifier"
    var items = [AccidentInstructionModel]()
    
    let manager = NetworkManager.instance
    let refreshView = UIRefreshControl()
    
    // paggination
    let perPage: Int = 10
    var currentPage: Int = 1
    var isDataLoading: Bool = true
    var firstStart: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helper.addRightLogoImageToNavigationItem(target: self, action: nil)
        self.title = "What to do if"
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        self.refreshView.addTarget(self, action: #selector(actionRefresh), for: .valueChanged)
        self.tableView.addSubview(refreshView)
        self.tableView.separatorStyle = .none
 
        Helper.showSpinner(self)
        fetchData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func actionRefresh() {
        self.refreshView.beginRefreshing()
        fetchData()
    }

    func fetchData() {
        manager.getAccidentInstructions(page: currentPage, per_page: perPage, query: "", sort_column: "title", sort_type: "asc") { (result) in
            // hide all indicators
            if self.firstStart == true {
                Helper.hideSpinner(self)
                self.tableView.separatorStyle = .singleLine
            } else {
                self.refreshView.endRefreshing()
            }
            
            if result != nil {
                var index = self.items.count + 2
                
                self.items.insert(contentsOf: result!, at: self.items.endIndex)
                self.tableView.reloadData()
                
                index = (index > self.items.count) ? self.items.count - 1 : index

                if !self.firstStart {
                    self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .middle, animated: true)
                } else {
                    self.firstStart = false
                }
                
                self.isDataLoading = false
            } else {
                Helper.showError(self, message: "Something went wrong :(")
            }
        }
    }
    
}


// MARK: UIScrollViewDelegate

extension WhatToDoTableViewController {

    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height {
            if !isDataLoading {
                isDataLoading = true
                self.currentPage += 1
                fetchData()
            }
        }
    }
}


// MARK: UITableViewDataSource

extension WhatToDoTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Configure the cell...
        cell.textLabel?.text = items[indexPath.row].title
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.numberOfLines = 2
        cell.selectionStyle = .none
    }
    
}


// MARK: UITableViewDelegate

extension WhatToDoTableViewController {
 
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabbedVC") as! TabbedViewController
        vc.model = self.items[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
