//
//  TabbedViewController.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class TabbedViewController: UIViewController {
    
    var model: AccidentInstructionModel?

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var infoTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helper.addRightLogoImageToNavigationItem(target: self, action: nil)

        self.title = self.model?.title
        if self.model?.tabs == false {
            self.segmentControl.removeSegment(at: 1, animated: false)
            self.segmentControl.setTitle(self.model?.tab_title1, forSegmentAt: 0)
        } else {
            self.segmentControl.setTitle(self.model?.tab_title1, forSegmentAt: 0)
            self.segmentControl.setTitle(self.model?.tab_title2, forSegmentAt: 1)
        }
        
        updateData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func actionChangeTab(_ sender: Any) {
        updateData()
    }
    
    func updateData() {
        if model == nil {
            return
        }
        
        var mutableAttr: NSMutableAttributedString?
        if self.segmentControl.selectedSegmentIndex == 0 {
            mutableAttr = NSMutableAttributedString(attributedString: (self.model?.tab_content1?.utf8Data?.attributedString)!)
        } else {
            mutableAttr = NSMutableAttributedString(attributedString: (self.model?.tab_content2?.utf8Data?.attributedString)!)
        }
        
        mutableAttr?.addAttributes([NSForegroundColorAttributeName: UIColor.appGeneralColor(), NSFontAttributeName: UIFont.systemFont(ofSize: 15.0)], range: NSMakeRange(0, mutableAttr!.length))
        self.infoTextView.attributedText = mutableAttr
    }
}
