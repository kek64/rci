//
//  ServiceInfoViewController.swift
//  RCI
//
//  Created by Admin on 01.04.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class ServiceInfoViewController: UIViewController {
    
    var model: ServiceModel?
    @IBOutlet weak var infoTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helper.addRightLogoImageToNavigationItem(target: self, action: nil)
        self.title = self.model?.title
        
        let mutableAttr = NSMutableAttributedString(attributedString: (self.model?.description?.utf8Data?.attributedString)!)
        mutableAttr.addAttributes([NSForegroundColorAttributeName: UIColor.appGeneralColor(), NSFontAttributeName: UIFont.systemFont(ofSize: 15.0)], range: NSMakeRange(0, mutableAttr.length))
        infoTextView.attributedText = mutableAttr
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func actionOpenWebSite(_ sender: Any) {
        Helper.openURL(urlString: self.model!.website!)
    }
    
}
