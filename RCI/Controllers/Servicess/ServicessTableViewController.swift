//
//  ServicessTableViewController.swift
//  RCI
//
//  Created by Admin on 27.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class ServicessTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.addRightLogoImageToNavigationItem(target: self, action: nil) //#selector(actionRightButton))
        self.title = "Services"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showBusiness() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ServicesListVC") as! ServicessListTableViewController
        vc.type = .Business
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func showPersonal() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ServicesListVC") as! ServicessListTableViewController
        vc.type = .Personal
        self.navigationController?.pushViewController(vc, animated: true)
    }

}


// MARK: UITableViewDelegate

extension ServicessTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            showBusiness()
            break
        case 1:
            showPersonal()
            break
        default:
            break
        }
    }
    
}

