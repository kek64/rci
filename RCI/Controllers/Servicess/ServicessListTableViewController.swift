//
//  PersonalTableViewController.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

enum ListType : Int {
    case Personal
    case Business
    
    func toString() -> String {
        switch self {
        case .Personal:
            return "personal"
        case .Business:
            return "business"
        }
    }
}

class ServicessListTableViewController: UITableViewController {
    
    let manager = NetworkManager.instance
    let refreshView = UIRefreshControl()
    
    // paggination
    let perPage: Int = 10
    var currentPage: Int = 1
    var isDataLoading: Bool = true
    
    let reuseIdentifier = "itemsReuseIdentifier"
    var items = [ServiceModel]()
    
    var firstStart: Bool = true
    var type: ListType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helper.addRightLogoImageToNavigationItem(target: self, action: nil)
        self.refreshView.addTarget(self, action: #selector(actionRefresh), for: .valueChanged)
        
        self.tableView.addSubview(refreshView)
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        self.tableView.separatorStyle = .none
        
        self.title = type!.toString().capitalized
        
        Helper.showSpinner(self)
        fetchData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func actionRefresh() {
        self.refreshView.beginRefreshing()
        fetchData()
    }
    
    func fetchData() {
        manager.getServices(page: currentPage, per_page: perPage, query: "", service_type: (self.type?.toString())!, sort_column: "title", sort_type: "asc") { (result) in
            
            // hide all indicators
            if self.firstStart == true {
                Helper.hideSpinner(self)
                self.tableView.separatorStyle = .singleLine
            } else {
                self.refreshView.endRefreshing()
            }
            
            if result != nil {
                var index = self.items.count + 2
                
                self.items.insert(contentsOf: result!, at: self.items.endIndex)
                self.tableView.reloadData()
                
                index = (index > self.items.count) ? self.items.count - 1 : index
                
                if !self.firstStart {
                    self.tableView.scrollToRow(at: IndexPath(row: index, section: 0), at: .middle, animated: true)
                } else {
                    self.firstStart = false
                }
                
                self.isDataLoading = false
            } else {
                Helper.showError(self, message: "Something went wrong :(")
            }
        }
    }
    
}


// MARK: UIScrollViewDelegate

extension ServicessListTableViewController {
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height {
            if !isDataLoading {
                isDataLoading = true
                self.currentPage += 1
                fetchData()
            }
        }
    }
}


// MARK: UITableViewDataSource

extension ServicessListTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Configure the cell...
        cell.textLabel?.text = items[indexPath.row].title
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.numberOfLines = 2
        cell.selectionStyle = .none
    }
}


// MARK: UITableViewDelegate

extension ServicessListTableViewController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ServiceInfoVC") as! ServiceInfoViewController
        vc.model = self.items[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
