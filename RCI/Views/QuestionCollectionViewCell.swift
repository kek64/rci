//
//  QuestionCollectionViewCell.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class QuestionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupShadows() {
        layer.masksToBounds = false
        layer.borderColor = UIColor.white.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowRadius = 0.5
        layer.shadowOffset = CGSize.zero
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    }
    
    static func estimateCellHeight(title: String, text: String) -> CGFloat {
        var height: CGFloat = 0.0
        let cellTopPadding: CGFloat = 12.0
        let cellBottomPadding: CGFloat = 14.0
        let width = UIScreen.main.bounds.size.width - 30.0
        height = estimateHeightForText(text: title, fontSize: 24, width: width)
        height += estimateHeightForText(text: text, fontSize: 16, width: width)
        height += cellTopPadding + cellBottomPadding
        return height
    }
    
    static func estimateHeightForText(text: String, fontSize: CGFloat, width: CGFloat) -> CGFloat {
        let font = UIFont.systemFont(ofSize: fontSize)
        let calcString = NSString(string: text)
        let textSize = calcString.boundingRect(with: CGSize(width: width , height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName : font], context: nil)
        return textSize.height
    }
}
