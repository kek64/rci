//
//  MainCollectionViewCell.swift
//  RCI
//
//  Created by Admin on 30.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit
import AVFoundation

class MainCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundImage.contentMode = .scaleAspectFill
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        backgroundImage.image = nil
    }

}
