//
//  AttachmentCollectionViewCell.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

protocol AttachmentCollectionViewCellDelegate {
    func actionDeleteAttachment(attachmentId: Int)
}

class AttachmentCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var backgroundImage: UIImageView!
    var attachmentId: Int = 0
    var delegate: AttachmentCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundImage.contentMode = .scaleAspectFill
    }

    @IBAction func actionDelete(_ sender: Any) {
        if delegate != nil {
            delegate?.actionDeleteAttachment(attachmentId: self.attachmentId)
        }
    }

}
