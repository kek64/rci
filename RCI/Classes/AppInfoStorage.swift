//
//  AppInfoStorage.swift
//  RCI
//
//  Created by Admin on 03.04.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import Foundation

class AppInfoStorage {
    
    // default values
    var phone: String = "tel://77777773"
    var royal_payment_website: String = "https://www.jccsmart.com/eBills/Welcome/Index/9634031"
    var e_nsured_website: String = "http://cw.royalcrowninsurance.eu/Login.aspx?ReturnUrl=%2f"
 
    static let instance = AppInfoStorage()
    private init() {}
    
    func downloadInfo() {
        NetworkManager.instance.getContactInfo { (result) in
            if result != nil {
                self.phone = "tel://" + (result?.phone)!
                self.royal_payment_website = (result?.royal_payment_website)!
                self.e_nsured_website = (result?.e_nsured_website)!
            }
        }
    }
}
