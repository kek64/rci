//
//  GeneralExtensions.swift
//  RCI
//
//  Created by Admin on 02.04.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import Foundation
import UIKit

// MARK: Data extension

extension Data {
    var attributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
}


// MARK: String extension

extension String {
    var utf8Data: Data? {
        return data(using: .utf8)
    }
}


// MARK: UIColor

extension UIColor {
    static func appGeneralColor() -> UIColor {
        return UIColor.init(colorLiteralRed: 48.0 / 255.0, green: 45.0 / 255.0, blue: 121.0 / 255.0, alpha: 1.0)
    }
}
