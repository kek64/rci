//
//  NetworkService.swift
//  RCI
//
//  Created by Admin on 27.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


// MARK: API constants

private let kBaseURL = "http://31.131.21.105:82"
private let kBranchesURL = "/api/v1/branches"
private let kAboutUsURL = "/api/v1/about_us"
private let kAboutRoyalAssistURL = "/api/v1/about_royal_assist"
private let kAccidentInstructionsURL = "/api/v1/accident_instructions"
private let kServices = "/api/v1/services"
private let kSaveAccidentReport = "/api/v1/accident_reports"
private let kQuestionnariesURL = "/api/v1/questionnaires"
private let kQuestionsListURL = "/api/v1/questions"
private let kQuestionnaireSubmitURL = "/api/v1/questionnaire_results"
private let kContactInfoURL = "/api/v1/contacts"

// MARK: NetworkManager

class NetworkManager {
    
    static let instance = NetworkManager()
    private init() {}
    
    
    func getBranches(completion: @escaping (_ result: [BranchModel]?) -> Void) {
        let responseURL = kBaseURL.appending(kBranchesURL)
        Alamofire.request(responseURL).validate().responseArray{ (response: DataResponse<[BranchModel]> ) in
            switch response.result {
            case .success:
                if let requestResult = response.result.value {
                    completion(requestResult)
                }
            case .failure:
                completion(nil)
            }
        }
    }
    
    func getAboutUs(completion: @escaping (_ result: TextInfoModel?) -> Void) {
        let responseURL = kBaseURL.appending(kAboutUsURL)
        Alamofire.request(responseURL).validate().responseObject { (response: DataResponse<TextInfoModel>) in
            switch response.result {
            case .success:
                if let requestResult = response.result.value {
                    completion(requestResult)
                }
            case .failure:
                completion(nil)
            }
        }
    }
    
    func getAboutRoyalAssist(completion: @escaping (_ result: TextInfoModel?) -> Void) {
        let responseURL = kBaseURL.appending(kAboutRoyalAssistURL)
        Alamofire.request(responseURL).validate().responseObject { (response: DataResponse<TextInfoModel>) in
            switch response.result {
            case .success:
                if let requestResult = response.result.value {
                    completion(requestResult)
                }
            case .failure:
                completion(nil)
            }
        }
    }
    
    // what to do if
    // sort_column - title, id, created_at
    // sort_type - asc, dsc
    func getAccidentInstructions(page: Int, per_page: Int, query: String, sort_column: String, sort_type: String, completion: @escaping ( _ result: [AccidentInstructionModel]?) -> Void) {
        let responseURL = kBaseURL.appendingFormat("%-12@?page=%ld&per_page=%ld&query=%-12@&sort_column=%-12@&sort_type=%-12@", kAccidentInstructionsURL, page, per_page, query, sort_column, sort_type)
  
        Alamofire.request(responseURL).validate().responseArray{ (response: DataResponse<[AccidentInstructionModel]> ) in
            switch response.result {
            case .success:
                if let requestResult = response.result.value {
                    completion(requestResult)
                }
            case .failure:
                completion(nil)
            }
        }
    }
    
    func getServices(page: Int, per_page: Int, query: String, service_type: String, sort_column: String, sort_type: String, completion: @escaping ( _ result: [ServiceModel]?) -> Void) {
        let responseURL = kBaseURL.appendingFormat("%-12@?page=%ld&per_page=%ld&query=%-12@&sort_column=%-12@&sort_type=%-12@&service_type%-12@", kServices, page, per_page, query, sort_column, sort_type, service_type)

        Alamofire.request(responseURL).validate().responseArray{ (response: DataResponse<[ServiceModel]> ) in
            switch response.result {
            case .success:
                if let requestResult = response.result.value {
                    completion(requestResult)
                }
            case .failure:
                completion(nil)
            }
        }
    }
    
    func getQuestionnaries(title: String, completion: @escaping(_ result: [QuestionnariesModel]?) -> Void) {
        let responseURL = kBaseURL.appendingFormat("%-12@?title=%-12@", kQuestionnariesURL, title)

        Alamofire.request(responseURL).validate().responseArray{ (response: DataResponse<[QuestionnariesModel]> ) in
            switch response.result {
            case .success:
                if let requestResult = response.result.value {
                    completion(requestResult)
                }
            case .failure:
                completion(nil)
            }
        }
    }
    
    func getQuestionsList(questtionaireId: Int, completion: @escaping(_ result: [QuestionsModel]?) -> Void) {
        let responseURL = kBaseURL.appendingFormat("%-12@?questionnaire_id=%ld", kQuestionsListURL, questtionaireId)

        Alamofire.request(responseURL).validate().responseArray{ (response: DataResponse<[QuestionsModel]> ) in
            switch response.result {
            case .success:
                if let requestResult = response.result.value {
                    completion(requestResult)
                }
            case .failure:
                completion(nil)
            }
        }
    }
    
    func submitQuestionnaireResults(completedQuestions: QuestionResultModel, completion: @escaping( _ result: QuestionnairesSubmitModel?) -> Void) {
        let responseURL = kBaseURL.appending(kQuestionnaireSubmitURL)
        let params: Parameters = completedQuestions.toJSON()
        
        Alamofire.request(responseURL, method: .post, parameters: params, encoding: JSONEncoding.default).validate().responseObject { (response: DataResponse<QuestionnairesSubmitModel> ) in
            switch response.result {
            case .success:
                if let requestResult = response.result.value {
                    completion(requestResult)
                }
            case .failure:
                completion(nil)
            }
        }
    }
    
    func getContactInfo(completion: @escaping(_ result: ContactInfoModel?) -> Void) {
        let responseURL = kBaseURL.appending(kContactInfoURL)
        
        Alamofire.request(responseURL).validate().responseObject{ (response: DataResponse<ContactInfoModel> ) in
            switch response.result {
            case .success:
                if let requestResult = response.result.value {
                    completion(requestResult)
                }
            case .failure:
                completion(nil)
            }
        }
    }
    
    // post
    func saveAccidentReport(name: String, reg_policy_number: String, phone: String, attachments: [UIImage], completion: @escaping ( _ result: TextInfoModel?) -> Void ) {
        
        // compress images
        
        //let responceURL = kBaseURL.appending(kSaveAccidentReport)
    }
    
    
}


