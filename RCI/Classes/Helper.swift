//
//  Helper.swift
//  RCI
//
//  Created by Admin on 30.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit
import MBProgressHUD

class Helper {
 
    static func addLogoImageToNavigationItem(_ navigationItem: UINavigationItem) {
        let logoImage = UIImageView(image: UIImage(named: "main_logo"))
        logoImage.contentMode = UIViewContentMode.scaleAspectFit
        navigationItem.titleView = logoImage
    }
    
    static func addRightLogoImageToNavigationItem(target: UIViewController, action: Selector?) {
        let logoImage = UIImage(named: "red_logo_icon")
        let rightButton = UIBarButtonItem(image: logoImage?.withRenderingMode(.alwaysOriginal), style: .plain, target: target, action: action)
        target.navigationItem.rightBarButtonItem = rightButton
    }

    static func openURL(urlString: String) {
        if let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    static func showSpinner(_ inViewController: UIViewController) {
        UIApplication.shared.beginIgnoringInteractionEvents()
        let progressHUD = MBProgressHUD.showAdded(to: inViewController.view, animated: true)
        progressHUD.mode = .indeterminate
    }
    
    static func hideSpinner(_ inViewController: UIViewController) {
        UIApplication.shared.endIgnoringInteractionEvents()
        MBProgressHUD.hide(for: inViewController.view, animated: true)
    }
    
    static func showError(_ inViewController: UIViewController, message: String) {
        let progressHUD = MBProgressHUD.showAdded(to: inViewController.view, animated: true)
        progressHUD.mode = .text
        progressHUD.label.text = message
        progressHUD.backgroundView.style = .solidColor
        progressHUD.hide(animated: true, afterDelay: 2.0)
    }
    
}
