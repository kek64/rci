//
//  MaterialTextField.swift
//  RCI
//
//  Created by Admin on 31.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

class MaterialTextField: UITextField {
    
    var lineWidth: Int = 0
    var canPaste: Bool = true

    override var tintColor: UIColor! {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        let beginOffset = CGPoint(x: rect.minX, y: rect.maxY)
        let endOffset = CGPoint(x: rect.maxX, y: rect.maxY)
        
        let path = UIBezierPath()
        
        path.move(to: beginOffset)
        path.addLine(to: endOffset)
        path.lineWidth = CGFloat(self.lineWidth)
        
        tintColor.setStroke()
        path.stroke()
    }
 
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if canPaste == false {
            if action == #selector(UIResponderStandardEditActions.paste(_:)) {
                return false
            }
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
